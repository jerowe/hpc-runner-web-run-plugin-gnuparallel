# NAME

HPC::Runner::Web::run::Plugin::GnuParallel - Blah blah blah

# SYNOPSIS

    use HPC::Runner::Web::run::Plugin::GnuParallel;

# DESCRIPTION

HPC::Runner::Web::run::Plugin::GnuParallel is

# AUTHOR

Jillian Rowe &lt;jillian.e.rowe@gmail.com>

# COPYRIGHT

Copyright 2015- Jillian Rowe

# LICENSE

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself.

# SEE ALSO
